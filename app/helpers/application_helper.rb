# -*- encoding : utf-8 -*-
# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  include Rack::Recaptcha::Helpers
  
  def title
    I18n.t :app_title
  end
  
  def meta_desc
    "Scholz bringt seit über 100 Jahren Farbe in Ihre Produkte. Farbpigmente für farbige Betonprodukte aller Art, farbigen Asphalt, Farben und Lacke, Kunststoffe, Dekorpapier und für viele andere Medien, die unser Leben bunter machen. #{APP_CONFIG[:keywords]}"
  end
  
  def keywords
    APP_CONFIG[:keywords]
  end
  
  def current_lang
    I18n.locale
  end
  
  def match_additional_params(options)
    match = false
    options.keys.each do |k|
      [*options[k]].each do |v| 
        break if (match = v.to_s == params[k])
      end
      return false unless match
    end
    match
  end
  
  def map_link(lat,long,path)
    link_to image_tag("http://maps.google.com/maps/api/staticmap?size=230x120&sensor=false&zoom=16&markers=#{lat.to_s}%2C#{long.to_s}"), path, :target => "_blank"
  end
    
  def link_to_unless_current(title, link, *args, &block)
    options = args.extract_options!
    # pfad zerlegen + controller test
    unless current_page?(link) || match_additional_params(options)
      link_to title, link, :class => options[:class]
    else
      content_tag :span, title, :class => 'current'
    end
  end
  
  def s3_image_path(filename)
    "#{APP_CONFIG[:s3_asset_url]}/#{filename}"
  end

  def s3_image_tag(filename, options={})
    image_tag(s3_image_path(filename), options)
  end
  
  def bereichs_subheader(bereich, options = {})
    options.reverse_merge!( :header => t(".header"))
    content_tag :h2, options[:header], :class => "bereich_#{bereich}_txt"
  end
  
  def mitte_bild(path)
    content_for :mitte_bild, image_tag(path, :alt => keywords)
  end
  
  def main_header(options = {})
    options.reverse_merge!(:header => t(".main_header"))
    content_for :main_header, options[:header]
  end
  
  def link_with_arrow(name, path, options = {})
     options.reverse_merge!(:bereich => 1)
     link_to name, path, :class => "arrow_link bereich_#{options[:bereich]}_arrow_e #{options[:class] if options[:class].present?}", :target => "#{options[:target] if options[:target].present?}"
  end
  
  def link_with_flag(name, path, options = {})
      options.reverse_merge!(:bereich => 1)
      link_to name, path, :class => "flag_link #{options[:class] if options[:class].present?}" 
  end    
      
  def link_to_top(options = {})
    options.reverse_merge!(:bereich => 1)
    content_tag :div, :style => "text-align:right;" do
      link_to t(:back_to_top), '#top', :class => "arrow_link small bereich_#{options[:bereich]}_arrow_n #{options[:class] if options[:class].present?}"
    end
  end
  
  def link_go_back(path, options = {})
    link_to t(:go_back), path, :class => "link_go_back #{options[:class] if options[:class].present?}"
  end
  
  def link_abort(path, options = {})
    link_to t(:abort), path, :class => "link_abort #{options[:class] if options[:class].present?}"
  end
  
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end
  
  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"))
  end
  
  def link_to_broschure(options = {})
    options.reverse_merge!(:icon => false, :language => 'de-en')
    link_to "Download Scholz-Unternehmensbroschüre #{options[:language].upcase} (PDF) ", "http://production.harold-scholz.de/pdfs/scholz-unternehmensbroschure-#{options[:language]}.pdf", :class => "#{'icon_link_to_pdf company_broschure' if options[:icon]}", :target => "_blank", :onClick => "_gaq.push(['_trackEvent','Scholz Broschuere #{options[:language]}','PDF',this.href]);"
  end
  
  def localized_attr_for(klass, attribute)
    klass.send(attribute.to_s + "_" + I18n.locale.to_s) if klass.send(attribute.to_s + "_" + I18n.locale.to_s)
  end
  
  def column_header_for(application_field,number)
    localized_attr_for(application_field.send('column_header_' + number.to_s),:title) if application_field.send('column_header_' + number.to_s)
  end
  
  def check_for(argument)
    image_tag("icons/check.png") if argument
  end
  
  def link_to_lanxess_shade_card(options = {})
    options.reverse_merge!(:bereich => 1)
    link_to "Lanxess-Farbtonkarte<br/>für die Bauindustrie (PDF)", "http://bayferrox.com/uploads/tx_lxsmatrix/Shadecard_Construction_en_de_2013.pdf", :class => "icon_link_to_pdf bauindustrie bereich_#{options[:bereich]}_txt", :target => "_blank", :onClick => "_gaq.push(['_trackEvent','Lanxess Farbtonkarte','PDF',this.href]);"
  end
  
  def link_to_lanxess_plastics_shade_card(options = {})
    options.reverse_merge!(:bereich => 1)
    link_to "Lanxess-Farbtonkarte<br/>für Kunststoffe (PDF)", "http://production.harold-scholz.de/pdfs/Global_Shadecard_plastics_en_de_2010_WEB_2_01.pdf", :class => "icon_link_to_pdf kunststoffe bereich_#{options[:bereich]}_txt", :target => "_blank", :onClick => "_gaq.push(['_trackEvent','Lanxess Farbtonkarte Kunststoffe','PDF',this.href]);"
  end
  
  def link_to_scholz_shade_card(options = {})
    options.reverse_merge!(:bereich => 1)
    link_to "Download Scholz-Farbtonkarte (PDF)", "http://production.harold-scholz.de/pdfs/scholz-farbtonkarte.pdf", :class => "icon_link_to_pdf bereich_#{options[:bereich]}_txt scholz_karte", :target => "_blank", :onClick => "_gaq.push(['_trackEvent','Scholz Farbtonkarte','PDF',this.href]);"
  end
  
  def link_to_scholz_pigment_preparations_card(options = {})
    options.reverse_merge!(:bereich => 1)
    link_to "Download Scholz-Farbtonkarte Pigmentpräparationen (PDF)", "http://production.harold-scholz.de/pdfs/pigmentpraeparationen.pdf", :class => "icon_link_to_pdf bereich_#{options[:bereich]}_txt scholz_karte_pigment_preparations", :target => "_blank", :onClick => "_gaq.push(['_trackEvent','Scholz Farbtonkarte','PDF',this.href]);"
  end
  
  def link_to_scholz_coatings(options = {})
    options.reverse_merge!(:bereich => 1)
    link_to "Dowload Lanxess-Farbtonkarte<br/>für Farben und Lacke (PDF)", "http://production.harold-scholz.de/pdfs/Shadecard_Coatings_High-Performance_en-de_2009_01.pdf", :class => "icon_link_to_pdf bereich_#{options[:bereich]}_txt farben_lacke", :target => "_blank", :onClick => "_gaq.push(['_trackEvent','Scholz Farben und Lacke','PDF',this.href]);"
  end
  
  def truncate_center(string,*args)
    return if string.blank?
    options = args.extract_options!
    unless args.empty?
      ActiveSupport::Deprecation.warn('truncate takes an option hash instead of separate ' +
        'length and omission arguments', caller)
      options[:length] = args[0] || 30
      options[:omission] = args[1] || "..."
    end
    options.reverse_merge!(:length => 30, :omission => "...")
    length = options[:length]
    if string
      string1= string[0...length/2]
      string2= string[string.length - length/2...string.length]
      return string1 + options[:omission] + string2
    end
  end
end
