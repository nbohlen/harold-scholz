# -*- encoding : utf-8 -*-
# Helper for inheriting layouts
module LayoutHelper            	
	# layouts  
	def parent_layout(parent_name, options = {}, &block)
	  path = template.path.split(/\//)
	  path.pop             
	  template_path = path.join('/')   
	  # remove layouts/
	  path.delete_at(0)
	  # add templatename :prefix/:tempate_name
	  path.push template.name               
	  # join to :prefix_:template_name
	  template_name_with_path_prefix = path.join('_')
		options.reverse_merge!({ :class => template_name_with_path_prefix })      
		add_to_content_classes(options[:class])
		options.reverse_merge!({ :content_class => parent_name.to_sym })
		if block_given?
			content_for(options[:content_class].to_sym, capture(&block))
			concat(render(:file => "#{template_path}/#{parent_name}"))
		else
		 	render(:file => "#{template_path}/#{parent_name}")     
		end
	end  
	
	private

	# sets the app title
	def add_to_content_classes(content_class)
		content_for(:content_classes) do
		  if defined?(@__content_class_first)
		    " #{content_class}" 
	    else
	      @__content_class_first = true
	      content_class
      end
		end
	end

end
