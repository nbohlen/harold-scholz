# -*- encoding : utf-8 -*-
module ProductHelper
  
  def color_cell(product,shade,application_field, color_name, product_group)
    style = "text-align:center;"
    style += "background-color:##{shade}; border: 1px solid #ccc;" if shade.present?
    style += "background-image: url('../images/#{application_field.permalink}_overlay.png')" if application_field.id == 2101 && shade.present? # Wood Plastic Composites
    content_tag :td, :width => "140", :style => style do
      inner = ""
      if shade.present?
        if product.url.present?
          inner += link_to image_tag("blank.gif", :width => "100%", :height => "50", :border => "0", :alt => APP_CONFIG[:keywords]), "#{product.url}?tx_editfiltersystem_pi1[name]=#{product.number}", :target => "_blank", :title => "#{product.number} (#{localized_attr_for(product_group, :title)}, #{localized_attr_for(color_name,:title)})"
        else
          inner += image_tag("blank2.gif", :width => "100%", :height => "50", :border => "0", :alt => APP_CONFIG[:keywords])
          inner
        end
      else
        I18n.t(:not_recommended) if application_field.id == 700
      end
    end
  end
end
