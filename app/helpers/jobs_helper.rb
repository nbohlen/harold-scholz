# -*- encoding : utf-8 -*-
module JobsHelper
  def show_jobs_size
    " (#{Job.active.size})" if Job.active.size > 0
  end
end
