# -*- encoding : utf-8 -*-
module DownloadHelper
  
  def publication_date(download)
    download.publication_date.strftime('%m/%y')
  end
  
  def asset_type(download)
    download.asset_content_type.split('/')[1].upcase
  end
  
  def download_title(download)
    title = ""
    title += "#{download.title}"
    title += ", #{download.issue}" if download.issue.present?
    title +=" (#{publication_date(download)})" if download.publication_date.present?
    title += " #{content_tag :span, download.language.upcase, :class => 'small'}" if download.language.present?
    title += " <span class='small'>[#{asset_type(download)}]</span>"
    return title
  end
end
