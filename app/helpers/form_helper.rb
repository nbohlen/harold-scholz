# -*- encoding : utf-8 -*-
module FormHelper
  
  def submit_button(text,f,options = ())
    content_tag :div, :class => "button_group #{options[:class] if options[:class].present?}" do
      html = ""
      unless options[:cancel_path].blank?
        content_tag :span do
          html += "#{link_abort options[:cancel_path]} oder "
        end
      end
      html += f.submit text
      html
    end
  end
  
end
