# -*- encoding : utf-8 -*-
module PostHelper

  #depricated! Use localized_attr_for instead
  def post_title(post)
    post.send("title_" + I18n.locale.to_s)
  end

  #depricated! Use localized_attr_for instead  
  def post_content(post)
    post.send("content_" + I18n.locale.to_s)
  end
  
  def newest_posts(locale, options = {})
    options.reverse_merge!(:length => 1)
    if locale == "de"
      Post.de.find(:all, :limit => options[:length])
    else
      Post.en.find(:all, :limit => options[:length])
    end
  end
  
end
