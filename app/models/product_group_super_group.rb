# -*- encoding : utf-8 -*-
class ProductGroupSuperGroup < ActiveRecord::Base
  belongs_to :product_group, :foreign_key => "super_group_id"
  belongs_to :super_group 
end
