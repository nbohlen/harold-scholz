# -*- encoding : utf-8 -*-
class ContactNotifier < ActionMailer::Base

  def contact_form(contact, request = nil)
    @subject = "Anfrage über Kontaktformular (Harold Scholz)"
    
    @body['nature']             = contact.nature             if contact.nature
    @body["company"]            = contact.company            if contact.company          
    @body["full_name"]          = contact.full_name          if contact.full_name        
    @body["street"]             = contact.street             if contact.street           
    @body["zip"]                = contact.zip                if contact.zip              
    @body["city"]               = contact.city               if contact.city             
    @body["country"]            = contact.country            if contact.country          
    @body["phone"]              = contact.phone              if contact.phone            
    @body["fax"]                = contact.fax                if contact.fax              
    @body["email"]              = contact.email              if contact.email            
    @body["industry"]           = contact.industry           if contact.industry         
    @body["interest"]           = contact.interest           if contact.interest
    @body['message']            = contact.message            if contact.message
                                            
    @recipients = "anfrage@harold-scholz.de"
    @from = contact.email
    @sent_on = Time.now
    @headers = {}
  end                                          

  

end
