# -*- encoding : utf-8 -*-
class Product < ActiveRecord::Base
  belongs_to :product_group
  belongs_to :color_name
  has_many :application_field_products
  has_many :application_fields, :through => :application_field_products
  
  validates_presence_of :number
  
  #named_scope :color_name, lambda{ |color_name_id| { :conditions => ['color_name = ?', color_name]} }
  named_scope :valid, :conditions => ["color_name_id IS NOT ? OR color_name_id != ?", nil, 0]
  
end
