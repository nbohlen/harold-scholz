# -*- encoding : utf-8 -*-
class SuperGroup < ActiveRecord::Base
  default_scope :order => "title_de ASC"
  
  has_permalink :title_de, :update => false
  
  # has_many :product_group_super_groups
  # has_many :product_groups, :through => :product_group_super_groups
  has_and_belongs_to_many :product_groups, :join_table => "product_group_super_groups"
  has_and_belongs_to_many :downloads, :order => "title ASC"
  
  accepts_nested_attributes_for :product_groups
  
  named_scope :ordered_name, :order => "title_de ASC"


  def to_param
    self.permalink
  end

end
