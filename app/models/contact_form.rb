# -*- encoding : utf-8 -*-
class ContactForm < ActiveRecord::Base
  
  def self.columns() @columns ||= []; end
  
  def self.column(name, sql_type = nil, default = nil, null = true)
    columns << ActiveRecord::ConnectionAdapters::Column.new(name.to_s, default, sql_type.to_s, null)
  end
  
  column :nature,            :string
  column :company,           :string
  column :full_name,         :string
  column :street,            :string
  column :zip,               :string
  column :city,              :string
  column :country,           :string
  column :phone,             :string
  column :fax,               :string
  column :email,             :string
  column :industry,          :string
  column :interest,          :string
  
  column :message,           :text

  validates_format_of   :email,   :with => /^[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i, :message => "^Sie haben keine gültige Email-Adresse angegeben."
  validates_length_of   :message, :maximum => 1000, :message => "^Ihre Nachricht ist mehr als 1000 Zeichen."
  validates_length_of   :full_name,    :maximum => 255, :message => "^Ihr Name ist länger als 255 Zeichen."
  validates_presence_of :message, :message => "^Nachricht muss ausgefüllt sein"
  validates_presence_of :full_name, :message => "^Geben Sie Ihren vollständigen Namen an."
  validates_presence_of :street, :message => "^Straße muss ausgefüllt sein."
  validates_presence_of :zip, :message => "^PLZ muss vollständig ausgefüllt sein."
  validates_presence_of :city, :message => "^Ort muss ausgefüllt sein."
  validates_presence_of :country, :message => "^Land muss ausgefüllt sein."
  validates_presence_of :email, :message => "^Bitte geben Sie Ihre Email-Adresse ein."
  validates_presence_of :phone, :message => "^Telefon muss ausgefüllt sein."
  
  INDUSTRIES = [
      "Farbe und Lack", 
      "Betondachsteine", 
      "Betonpflaster/Erdfeuchter Beton", 
      "Putze/Mörtel", 
      "Industriefußböden", 
      "Asphalt", 
      "Baustoffhandel", 
      "Kunststoff", 
      "Papier", 
      "Keramik", 
      "Gummi", 
      "Fliesen und Verarbeitungsmaterial", 
      "Glas", 
      "Gießerei", 
      "Ziegelwerke/Klinker", 
      "Baustoffe", 
      "Ortbeton", 
      "Holzhäcksel", 
      "Betonfertigteile", 
      "Objekte Fertigteile", 
      "Objekte Ortbeton", 
      "Titandioxid",
      "Sonstige" 
    ]
    
  INTERESTS = [
      "Bayscape", 
      "Bayferrox Pulver (Eisenoxid)",
      "Chrom Farbslurry",
      "Chromoxide",
      "Dachsteinfarben",
      "Dosieranlagen",
      "Lohnfertigung",
      "Eisenoxid Farbslurry",
      "Pigmentpräpäration",
      "Flüssigschwarz",
      "Kasslerbraun",
      "Mischphasenpigmente",
      "Nußbeize",
      "Pulverruß",
      "Titan Farbslurry",
      "Titandioxid",
      "Ultramarin",
      "Universalpasten",
      "Sonstige" 
    ]
  
end
