# -*- encoding : utf-8 -*-
class ApplicationField < ActiveRecord::Base
  acts_as_tree :order => "position"
  acts_as_list :scope => :parent_id
  has_permalink :title_de, :update => true
  
  belongs_to :column_header_1, :class_name => "ColumnHeader", :foreign_key => :column_header_1_id
  belongs_to :column_header_2, :class_name => "ColumnHeader", :foreign_key => :column_header_2_id
  
  has_many :application_field_products
  has_many :products, :through => :application_field_products
  has_and_belongs_to_many :downloads, :order => "title ASC"

  #named_scope :roots, :conditions => { :parent_id => nil}  
  named_scope :roots, :conditions => ["parent_id IS ? OR parent_id = ?", nil,0 ]
  named_scope :subs, :conditions => ["parent_id IS NOT ? OR parent_id != ?", nil, 0]
  named_scope :ordered, :order => "position"
  named_scope :ordered_name, :order => "title_de ASC"
  named_scope :ordered_by_parent_id, :order => "parent_id ASC"
  def to_param
    self.permalink
  end

  
end
