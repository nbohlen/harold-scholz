# -*- encoding : utf-8 -*-
class PeopleCategory < ActiveRecord::Base
  acts_as_tree :order => "position"
  acts_as_list :scope => :parent_id
  
  has_many :people
  
  named_scope :companies, :conditions => { :parent_id => nil }, :include => :people
  
  named_scope :departments, :conditions => ["parent_id IS NOT ? OR parent_id != ?",nil, 0]
end
