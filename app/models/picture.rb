# -*- encoding : utf-8 -*-
class Picture < ActiveRecord::Base
  belongs_to :post
  
  has_attached_file :picture,
                    :styles => { 
                      :icon   => ["20x20#", :jpg], 
                      :thumb  => ["80x80>", :jpg], 
                      :normal => ["640x640>", :jpg] 
                      }
                                      
  validates_attachment_size :picture, :less_than => 5.megabytes
  validates_attachment_content_type :picture, :content_type => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png', 'image/gif', 'image/jpg', 'application/pdf']
  
end
