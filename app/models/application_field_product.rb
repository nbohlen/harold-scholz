# -*- encoding : utf-8 -*-
class ApplicationFieldProduct < ActiveRecord::Base
  belongs_to :product
  belongs_to :application_field
end
