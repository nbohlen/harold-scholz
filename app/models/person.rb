# -*- encoding : utf-8 -*-
class Person < ActiveRecord::Base
  belongs_to :people_category
  acts_as_list :scope => :people_category
  
  has_attached_file :picture,
                    :storage => :s3,
                    :styles => { :thumb => "25x25#", :normal => "90x120>" },
                    :s3_credentials => "#{RAILS_ROOT}/config/s3.yml",
                    :url => ':s3_path_url',
                    :path => "/people/:id/:id_:style.:extension",
                    :s3_permissions => 'public-read'
                    
  validates_attachment_size :picture, :less_than => 2.megabytes
  validates_attachment_content_type :picture, :content_type => ['image/jpeg', 'image/png', 'image/gif']
  validates_attachment_presence :picture, :message => "^Es muss ein Bildpfad angebeben sein."
  
  validates_presence_of :full_name, :message => "^Name muss angegeben werden"
  validates_presence_of :designation_de, :designation_en, :message => "^Die Bezeichnung muss für beide Sprachen eingegeben werden."
  
  named_scope :ordered, :order => "position"
  
end
