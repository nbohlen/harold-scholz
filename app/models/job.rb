# -*- encoding : utf-8 -*-
class Job < ActiveRecord::Base
  has_one :download
  
  named_scope :valid, :conditions => ["active IS ?", true]
  named_scope :de, :conditions => ["title_de IS NOT ? AND title_de != ?", nil,'']
  named_scope :en, :conditions => ["title_en IS NOT ? AND title_en != ?", nil,'']

  validates_presence_of :title_de, :body_de, :region, :author
  
  def active?
    self.active
  end
  
end
