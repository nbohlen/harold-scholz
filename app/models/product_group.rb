# -*- encoding : utf-8 -*-
class ProductGroup < ActiveRecord::Base
  has_permalink :title_de, :update => true
  
  has_many :products
  named_scope :ordered, :order => "title_de"
  named_scope :ordered_name, :order => "title_de ASC"
  
  # has_many :product_group_super_groups
  # has_many :super_groups, :through => :product_group_super_groups
  
  has_and_belongs_to_many :super_groups, :join_table => "product_group_super_groups"
  
  def localized_title
    self.send("title_" + I18n.locale.to_s)
  end
end
