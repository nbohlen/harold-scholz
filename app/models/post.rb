# -*- encoding : utf-8 -*-
class Post < ActiveRecord::Base
  has_many :pictures, :dependent => :destroy
  has_permalink :title_de, :update => true
  
  accepts_nested_attributes_for :pictures, :allow_destroy => true
  
  validates_presence_of :author, :date
  
  named_scope :de, :conditions => ["title_de IS NOT ? AND title_de != ?", nil,''], :order => "date DESC"
  named_scope :en, :conditions => ["title_en IS NOT ? AND title_en != ?", nil,''], :order => "date DESC"
  
  def to_param
    "#{self.id}-#{self.permalink}"
  end
end
