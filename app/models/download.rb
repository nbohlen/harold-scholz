# -*- encoding : utf-8 -*-
class Download < ActiveRecord::Base
  has_and_belongs_to_many :application_fields
  has_and_belongs_to_many :super_groups
  
  acts_as_taggable
  
  has_attached_file :asset,
                    :storage => :s3,
                    :url => ':s3_path_url',
                    :s3_credentials => "#{RAILS_ROOT}/config/s3.yml",
                    :path => "/downloads/:id/:basename.:extension",
                    :s3_permissions => 'public-read'
                    

                    
  named_scope :de, :conditions => [ "language LIKE ?", "%de%"]
  named_scope :en, :conditions => [ "language LIKE ?", "%en%"]
  named_scope :limit, lambda { |num| { :limit => num } }
  
  
  validates_uniqueness_of :asset_file_name, :message => "^Eine Datei mit diesem Namen ist schon hochgeladen."
  validates_presence_of :title, :message => "^Sie müssen einen Namen für die Datei eingeben."
  
  validates_attachment_presence :asset, :message => "^Es muss ein dateipfad angebeben sein."
  validates_attachment_size :asset, :less_than => 15.megabytes, :message => "^Die Datei ist größer als die erlaubten 15 Megabytes"
  validates_attachment_content_type :asset, :content_type => ['application/pdf','application/msword','application/x-pdf','application/x-doc','image/jpeg', 'image/png', 'image/gif']
  
end
