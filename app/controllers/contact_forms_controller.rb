# -*- encoding : utf-8 -*-
class ContactFormsController < ApplicationController
  
  before_filter :load_bereich
  
  def show
    @contact_form = ContactForm.new
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @contact_form = ContactForm.new(params[:contact_form])
    if @contact_form.valid? && recaptcha_valid?
      ContactNotifier.deliver_contact_form(@contact_form)
      flash[:notice] = "Vielen Dank! Ihre Anfrage wurde erfolgreich an uns verschickt."
      redirect_to root_url
    else
      if !recaptcha_valid?
        @contact_form.errors.add(:recaptcha, "ist nicht ausgefüllt.")
        # flash.now[:notice] = "Test error"
      end
      render :action => 'show'
    end
  end
  
  protected
  
  def load_bereich
    @bereich = 1
  end
  
end
