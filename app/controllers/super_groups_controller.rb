# -*- encoding : utf-8 -*-
class SuperGroupsController < ApplicationController
  before_filter :load_bereich
  
  def index
    @super_groups = SuperGroup.all
    
    respond_to do |wants|
      wants.html {}
    end
  end
  
  def show
    @super_group = SuperGroup.find_by_permalink(params[:id], :include => :product_groups)
    @product_groups = @super_group.product_groups.find(:all, :include => :products, :order => "title_de ASC")
    respond_to do |wants|
      wants.html {}
    end
  end
  
  def load_bereich
    @bereich = 2
  end
end
