# -*- encoding : utf-8 -*-
class ProductRangeController < ApplicationController
  before_filter :load_bereich
  
  def colour_pigments
    @product_groups = ProductGroup.ordered.all
  
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def load_bereich
    @bereich = 2
  end
end
