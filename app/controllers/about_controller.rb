# -*- encoding : utf-8 -*-
class AboutController < ApplicationController
  
  before_filter :load_bereich
    
  #home
  def index
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def contact
    respond_to do |wants|
      wants.html {}
    end
  end
  
  protected
  
  def load_bereich
    @bereich = 1
  end
  
end
