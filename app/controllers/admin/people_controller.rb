# -*- encoding : utf-8 -*-
class Admin::PeopleController < AdminController
  
  def index
    @companies = PeopleCategory.companies
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def new
    @person = Person.new
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @person = Person.new(params[:person])
    
    respond_to do |wants|
      if @person.save
        flash[:notice] = "Der Mitarbeiter wurde erfolgreich erstellt."
        wants.html { redirect_to admin_people_path }
      else
        flash[:notice] = "Der Mitarbeiter konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end
  
  def edit
    @person = Person.find(params[:id])
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def update
    @person = Person.find(params[:id])
    
    respond_to do |wants|
      if @person.update_attributes(params[:person])
        wants.html { redirect_to admin_people_path }
      end
    end
  end
  
  def move_higher
    person = Person.find(params[:id])
    person.move_higher
    person.save
    respond_to do |wants|
      wants.html { redirect_to admin_people_path }
    end
  end
  
  def move_lower
    person = Person.find(params[:id])
    person.move_lower
    person.save
    respond_to do |wants|
      wants.html { redirect_to admin_people_path }
    end
  end
  
  #def sort
  #  people_categorie = PeopleCategory.find(params[:people_category_id])
  #  params[:person].each_with_index do |id, index|
  #    people_categorie.people.update_all(['position=?', index+1], ['id=?', id])
  #  end
  #  render :nothing => true
  #end
  
  def destroy
    @person = Person.find(params[:id])
    @person.destroy
    
    respond_to do |wants|
      wants.html { redirect_to admin_people_path}
    end
  end
  
end
