# -*- encoding : utf-8 -*-
class Admin::ApplicationFieldsController < AdminController

  def index
    @application_fields = ApplicationField.paginate :per_page => 50, :page => params[:page], :order => "position"

    respond_to do |wants|
      wants.html { }
      wants.csv { }
    end
  end

  def new
    @application_field = ApplicationField.new

    respond_to do |wants|
      wants.html {  }
    end
  end

  def create
    @application_field = ApplicationField.new(params[:application_field])

    respond_to do |wants|
      if @application_field.save
        flash[:notice] = "Die Produktgruppe wurde erfolgreich erstellt."
        wants.html { redirect_to admin_application_fields_path }
      else
        flash[:notice] = "Die Produktgruppe konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end

  def edit
    @application_field = ApplicationField.find_by_permalink(params[:id])

    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def move_higher
    application_field = ApplicationField.find(params[:id])
    application_field.move_higher
    application_field.save
    respond_to do |wants|
      wants.html { redirect_to admin_application_fields_path }
    end
  end
  
  def move_lower
    application_field = ApplicationField.find(params[:id])
    application_field.move_lower
    application_field.save
    respond_to do |wants|
      wants.html { redirect_to admin_application_fields_path }
    end
  end

  def update
    @application_field = ApplicationField.find_by_permalink(params[:id])

    respond_to do |wants|
      if @application_field.update_attributes(params[:application_field])
        flash[:notice] = "Ihr Eintrag wurde erfolgreich aktualisiert."
        wants.html { redirect_to admin_application_fields_path }
      else
        flash[:notice] = "Ihr Eintrag wurde nicht aktualisiert."
        wants.html { redirect_to admin_application_fields_path }
      end
    end
  end

  def destroy
    @application_field = ApplicationField.find_by_permalink(params[:id])
    @application_field.destroy
  
    respond_to do |wants|
      wants.html { redirect_to admin_application_fields_path}
    end
  end

end
