# -*- encoding : utf-8 -*-
class Admin::ApplicationFieldProductsController < AdminController

  def index
    @application_field_products = ApplicationFieldProduct.all

    respond_to do |wants|
      wants.csv { }
    end
  end

end
