# -*- encoding : utf-8 -*-
class Admin::ProductsController < AdminController
  
  def index
    @search = Product.search(params[:search])
    @products = @search.paginate :per_page => 50, :page => params[:page]
    
    respond_to do |wants|
      wants.html {  }
      wants.csv { }
    end
  end
  
  def new
    @product = Product.new
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @product = Product.new(params[:product])
    
    respond_to do |wants|
      if @product.save
        flash[:notice] = "Das Produkt wurde erfolgreich erstellt."
        wants.html { redirect_to edit_admin_product_path(@product) }
      else
        flash[:notice] = "Das Produkt konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end
  
  def edit
    @product = Product.find(params[:id])
    
    respond_to do |wants|
      wants.html { }
    end
  end
  
  def update
    @product = Product.find(params[:id])
    
    respond_to do |wants|
      if @product.update_attributes(params[:product])
        flash[:notice] = "Das Produkt wurde erfolgreich aktualisiert."
        wants.html { redirect_to edit_admin_product_path(@product) }
      end
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    
    respond_to do |wants|
      wants.html { redirect_to admin_products_path}
    end
  end
  
end
