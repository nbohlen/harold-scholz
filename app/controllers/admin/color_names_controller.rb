# -*- encoding : utf-8 -*-
class Admin::ColorNamesController < AdminController

  def index
    @color_names = ColorName.all(:order => "id ASC")

    respond_to do |wants|
      wants.csv { }
    end
  end

end
