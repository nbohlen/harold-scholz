# -*- encoding : utf-8 -*-
class Admin::DownloadsController < AdminController
  
  def index
    @downloads = Download.paginate :per_page => 50, :page => params[:page]
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def new
    @download = Download.new
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @download = Download.new(params[:download])
    
    respond_to do |wants|
      if @download.save
        flash[:notice] = "Download wurde erfolgreich erstellt."
        wants.html { redirect_to admin_downloads_path }
      else
        flash[:notice] = "Download konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end
  
  def edit
    @download = Download.find(params[:id])
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def update
    @download = Download.find(params[:id])
    
    respond_to do |wants|
      if @download.update_attributes(params[:download])
        wants.html { redirect_to admin_downloads_path }
      end
    end
  end
  
  def destroy
    @download = Download.find(params[:id])
    @download.destroy
    
    respond_to do |wants|
      wants.html { redirect_to admin_downloads_path}
    end
  end
  
end
