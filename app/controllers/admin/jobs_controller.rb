# -*- encoding : utf-8 -*-
class Admin::JobsController < AdminController
  
  def index
    @jobs = Job.paginate :per_page => 25, :order => "created_at DESC", :page => params[:page]
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def new
    @job = Job.new
    # @job.pictures.build
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @job = Job.new(params[:job])
    
    respond_to do |wants|
      if @job.save
        flash[:notice] = "Die Stellenausschreibung wurde erfolgreich erstellt."
        wants.html { redirect_to admin_jobs_path }
      else
        flash[:notice] = "Die Stellenausschreibung konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end
  
  def edit
    @job = Job.find(params[:id])
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def update
    @job = Job.find(params[:id])
    
    respond_to do |wants|
      if @job.update_attributes(params[:job])
        wants.html { redirect_to admin_jobs_path }
      end
    end
  end
  
  def destroy
    @job = Job.find(params[:id])
    @job.destroy
    
    respond_to do |wants|
      wants.html { redirect_to admin_jobs_path}
    end
  end
  
end
