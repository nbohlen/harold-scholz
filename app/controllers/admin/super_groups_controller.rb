# -*- encoding : utf-8 -*-
class Admin::SuperGroupsController < AdminController
  
  def index
    @super_groups = SuperGroup.paginate :per_page => 50, :page => params[:page], :order => "title_de ASC"
    
    respond_to do |wants|
      wants.html { }
      wants.csv { }
    end
  end
  
  def new
    @super_group = SuperGroup.new
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @super_group = SuperGroup.new(params[:super_group])
    
    respond_to do |wants|
      if @super_group.save
        flash[:notice] = "Die Supergruppe wurde erfolgreich erstellt."
        wants.html { redirect_to admin_super_groups_path }
      else
        flash[:notice] = "Die Supergruppe konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end
  
  def edit
    @super_group = SuperGroup.find_by_permalink(params[:id])
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def update
    @super_group = SuperGroup.find_by_permalink(params[:id])
    
    respond_to do |wants|
      if @super_group.update_attributes(params[:super_group])
        flash[:notice] = "Ihr Eintrag wurde erfolgreich aktualisiert."
        wants.html { redirect_to edit_admin_super_group_path(@supergroup) }
      else
        flash[:notice] = "Ihr Eintrag wurde nicht aktualisiert."
        wants.html { redirect_to edit_admin_super_group_path(@supergroup) }
      end
    end
  end
  
  def destroy
    @super_group = SuperGroup.find_by_permalink(params[:id])
    @super_group.destroy
    
    respond_to do |wants|
      wants.html { redirect_to admin_super_groups_path}
    end
  end
  
end
