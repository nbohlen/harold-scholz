# -*- encoding : utf-8 -*-
class Admin::ProductGroupsController < AdminController
  
  def index
    @product_groups = ProductGroup.paginate :per_page => 50, :page => params[:page]
    
    respond_to do |wants|
      wants.html { }
      wants.csv { }
    end
  end
  
  def new
    @product_group = ProductGroup.new
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @product_group = ProductGroup.new(params[:product_group])
    
    respond_to do |wants|
      if @product_group.save
        flash[:notice] = "Die Produktgruppe wurde erfolgreich erstellt."
        wants.html { redirect_to admin_product_groups_path }
      else
        flash[:notice] = "Die Produktgruppe konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end
  
  def edit
    @product_group = ProductGroup.find(params[:id])
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def update
    @product_group = ProductGroup.find(params[:id])
    
    respond_to do |wants|
      if @product_group.update_attributes(params[:product_group])
        flash[:notice] = "Ihr Eintrag wurde erfolgreich aktualisiert."
        wants.html { redirect_to admin_product_groups_path }
      else
        flash[:notice] = "Ihr Eintrag wurde nicht aktualisiert."
        wants.html { redirect_to admin_product_groups_path }
      end
    end
  end
  
  def destroy
    @product_group = ProductGroup.find(params[:id])
    @product_group.destroy
    
    respond_to do |wants|
      wants.html { redirect_to admin_product_groups_path}
    end
  end
  
end
