# -*- encoding : utf-8 -*-
class Admin::PostsController < AdminController
  
  def index
    @posts = Post.paginate :per_page => 25, :order => "date DESC", :page => params[:page]
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def new
    @post = Post.new
    @post.pictures.build
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def create
    @post = Post.new(params[:post])
    
    respond_to do |wants|
      if @post.save
        flash[:notice] = "Die News wurde erfolgreich erstellt."
        wants.html { redirect_to admin_posts_path }
      else
        flash[:notice] = "Die News konnte nicht gespeichert werden."
        wants.html { render :new }
      end
    end
  end
  
  def edit
    @post = Post.find(params[:id])
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def update
    @post = Post.find(params[:id])
    
    respond_to do |wants|
      if @post.update_attributes(params[:post])
        wants.html { redirect_to admin_posts_path }
      end
    end
  end
  
  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    
    respond_to do |wants|
      wants.html { redirect_to admin_posts_path}
    end
  end
  
end
