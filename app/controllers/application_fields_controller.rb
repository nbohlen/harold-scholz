# -*- encoding : utf-8 -*-
class ApplicationFieldsController < ApplicationController
  before_filter :load_bereich
  
  def show
    @application_field = ApplicationField.find_by_permalink(params[:id])
    if @application_field
      @products = @application_field.products
      @product_groups = @application_field.products.map {|p| p.product_group}.uniq.sort_by {|pg| pg.title_de}
    end
    respond_to do |wants|
      wants.html {  }
      wants.php { redirect_to root_path }
    end
  end
  
  def load_bereich
    @bereich = 3
  end
end
