# -*- encoding : utf-8 -*-
class PeopleController < ApplicationController
  before_filter :load_bereich  
  
  def index
    @companies = PeopleCategory.companies
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def load_bereich
    @bereich = 1
  end
  
end
