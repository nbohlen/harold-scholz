# -*- encoding : utf-8 -*-
class JobsController < ApplicationController
  before_filter :load_bereich
  
  def index
    @jobs = Job.active
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def show
    @job = Job.find(params[:id])
  end
  
  def load_bereich
    @bereich = 1
  end
  
end
