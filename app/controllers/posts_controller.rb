# -*- encoding : utf-8 -*-
class PostsController < ApplicationController
  before_filter :load_bereich
  
  def index
    if current_locale?(:de)
      @posts = Post.de
    else
      @posts = Post.en
    end
    
    respond_to do |wants|
      wants.html {  }
    end
  end
  
  def show
    @post = Post.find(params[:id])
  end
  
  def load_bereich
    @bereich = 1
  end
  
end
