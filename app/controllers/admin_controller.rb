# -*- encoding : utf-8 -*-
class AdminController < ApplicationController
  #ensure_user_is_logged_in
  before_filter :login_required
  
  layout 'admin'
  
end
