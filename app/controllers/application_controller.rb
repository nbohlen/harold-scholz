# -*- encoding : utf-8 -*-
# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details
  include AuthenticatedSystem
  include Rack::Recaptcha::Helpers
  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password
  
  before_filter :authenticate if Rails.env.staging?

  
  #before_filter :set_locale 
  before_filter :set_locale_from_url
  
  def current_locale?(locale) 
    I18n.locale.to_s == locale.to_s
  end
  
  protected

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "scholz" && password == "1234"
    end
  end
end
