# -*- encoding : utf-8 -*-
class ContactFormBuilder < ActionView::Helpers::FormBuilder
    
  def text_field(attribute, options={})
    t = I18n.t("contact_forms.labels.#{attribute}")
    @template.content_tag(:p, label(attribute, t) + super )
  end
  
  def text_area(attribute, options={})
    t = I18n.t("contact_forms.labels.#{attribute}")
    @template.content_tag(:p, label(attribute, t) + super )
  end
  
end
