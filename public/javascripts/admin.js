function remove_fields(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".fields").hide();
  }

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $(link).parent().before(content.replace(regexp, new_id));
}


$(document).ready(function(){
	
	$(function(){
		
		//admin list item function
		
    $(".list_item .button_group a").css("display","none");
		
    $(".list_item").hover(

      function(){
        $(this).find("td.button_group a").fadeIn(300).parent().parent().addClass("edit_list_item");
      },

      function(){
        $(this).find("td.button_group a").hide().parent().parent().removeClass("edit_list_item");
      });
		
		//absolute logo positioning function
		
		var logo = $(".logo");
		var tableDimension = $("td.left");
		var posLeft = (Math.floor(tableDimension.width()+80));
		logo.css('left', posLeft+ 'px');
		
		$(window).resize(function(){
			$(logo);
			$(tableDimension);
			var posLeft = (Math.floor(tableDimension.width()+80));
			logo.css('left', posLeft+ 'px');
		});
	
		//fancybox function
	
		$("a.view_website").fancybox({
				'width': '90%',
				'height': '90%',
		    'autoScale': false,
		    'transitionIn': 'none',
				'transitionOut': 'none',
				'type': 'iframe'
	 	});
	 	
	 	// tags
		$('.tags a.tag').click(function(event){
		  var tag_name = $(this).attr('data-tag');
		  if($('input#download_tag_list').val() == ""){
		    var separator = ""
		  }else{
		    var separator = ","
		  };

		  $('input#download_tag_list').val(function( index, value ) {
        return value + separator + tag_name;
      });
      // $('input#download_tag_list').value += "test";
		  return false;
		})
		
		//colorpicker function
		
		$('input.colorfield').ColorPicker({
	  	onSubmit: function(hsb, hex, rgb, el) {
	  		$(el).val(hex);
	  		$(el).ColorPickerHide();
				$(el).parent().find(".color_preview").css('backgroundColor', '#' + hex);
				$(el).next().removeClass("active");
	  	},
	  	onBeforeShow: function () {
	  		$(this).ColorPickerSetColor(this.value);
	  	},
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				$(this).next().addClass("active");
				return false;
			},
			onChange: function (hsb, hex, rgb, el) {
				$('#colorSelector div').css('backgroundColor', '#' + hex);
				$(".active").css('backgroundColor', '#' + hex);
			},
			onHide: function(colpkr) {  
				return false;
			}
	  })
	  .bind('keyup', function(){
	  	$(this).ColorPickerSetColor(this.value);
	  });

  });
  
});