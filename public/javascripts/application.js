// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

document.createElement("nav");
document.createElement("header");
document.createElement("footer");
document.createElement("section");
document.createElement("aside");
document.createElement("article");

function remove_fields(link) {
    $(link).prev("input[type=hidden]").val("1");
    $(link).closest(".fields").hide();
  }

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $(link).parent().before(content.replace(regexp, new_id));
}

$(document).ready(function(){
	
	$('html').click(function() {
    $('.super_group_product_groups > ul > li .application_fields').hide();
    $('.super_group_product_groups > ul > li > a.active').removeClass('active');
    $('.super_group_product_groups > ul > li.active').removeClass('active');
  });
	
	$('.super_group_product_groups > ul > li > a').click(function(event){
	  $('.super_group_product_groups ul li .application_fields').hide();
	  $(this).parent().addClass('active');
	  $(this).addClass('active').nextAll('.application_fields').toggle();
	  event.stopPropagation();
	  return false;
	})
	
	//implementation fancybox
	
	$("a.view_image").fancybox({
		'titlePosition': 'inside'
		});
		
	$("a.view_map").fancybox({
			'width': '90%',
			'height': '90%',
	    'autoScale': false,
	    'transitionIn': 'none',
			'transitionOut': 'none',
			'type': 'iframe'
		});
	
	//functions for main navigation
	
	$("#main_nav li ul").css("display","none");
	
  $("#main_nav li a").click(function(){
   $(this).next().toggle();
   // return false
  });
	
	$("#main_nav #bereich_3 .sub_menu li:has(ul)").click(function(){
		$(this).children().first().toggleClass("active_item");
	});
	
	$(".sub_menu_2 li:has(span)").parent().prev().addClass("active_item");
	
	$("#main_nav li ul:has(span)").css("display","block");
	
	//show current url in print css function
	
	var $url = $(location).attr('href');
	var $obj = $('<span id="current_link">'+$url+'</span>');
  
  $(".link_print").click(function(){
    $("#content").append($obj);
  });
  
  $("input.required, textarea.required, select.required").after("<span class='required'>*</span>")

  // Hauptkeywords in alt-attribute der img tags setzen
  
  $('#accordion').accordion({
				active: false,
				header: '.heading',
				selectedClass: 'active', 
				navigation: true,
				event: 'click',
				autoheight: false,
				animated: 'easeslide'
			});

	$("#accordion ul li:nth-child(2n+1):not(.tag)").addClass("highlight_list");

});