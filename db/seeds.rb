# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#   
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Major.create(:name => 'Daley', :city => cities.first)

require 'active_record/fixtures'

# puts "Loading Fixtures"

# Fixtures.create_fixtures( "#{Rails.root}/test/fixtures", "people_categories" )
# Fixtures.create_fixtures( "#{Rails.root}/test/fixtures", "people" )
# Fixtures.create_fixtures( "#{Rails.root}/test/fixtures", "posts" )
# Fixtures.create_fixtures( "#{Rails.root}/test/fixtures", "downloads" )

puts "Loading .csv seeds"

Dir[Rails.root.join("db/seeds", "*.{yml,csv}")].each do |file|
  Fixtures.create_fixtures("db/seeds", File.basename(file, '.*'))
end

# if ENV["RAILS_ENV"] == 'development' 
#   puts "copying assets to system"
#   system "cp -r #{RAILS_ROOT}/db/assets/* #{RAILS_ROOT}/public/system/"  
# end
