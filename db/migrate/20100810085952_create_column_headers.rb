# -*- encoding : utf-8 -*-
class CreateColumnHeaders < ActiveRecord::Migration
  def self.up
    create_table :column_headers do |t|
      t.string :title_de
      t.string :title_en
      
      t.timestamps
    end
  end

  def self.down
    drop_table :column_headers
  end
end
