# -*- encoding : utf-8 -*-
class CreateProductGroupSuperGroups < ActiveRecord::Migration
  def self.up
    create_table :product_group_super_groups, :id => false do |t|
      t.integer :product_group_id
      t.integer :super_group_id
      
      t.timestamps
    end
    
    add_index :product_group_super_groups, [:product_group_id, :super_group_id], :unique => true
  end

  def self.down
    drop_table :product_group_super_groups
  end
end
