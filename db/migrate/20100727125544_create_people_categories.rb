# -*- encoding : utf-8 -*-
class CreatePeopleCategories < ActiveRecord::Migration
  def self.up
    create_table :people_categories do |t|
      t.string   :name_de
      t.string   :name_en
      t.integer  :parent_id
      t.integer  :position
      
      t.timestamps
    end
  end

  def self.down
    drop_table :people_categories
  end
end
