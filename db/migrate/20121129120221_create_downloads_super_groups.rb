# -*- encoding : utf-8 -*-
class CreateDownloadsSuperGroups < ActiveRecord::Migration
  def self.up
    create_table :downloads_super_groups, :id => false do |t|
      t.integer :download_id
      t.integer :super_group_id
    end
    
    add_index :downloads_super_groups, [:download_id, :super_group_id], :unique => true
    
  end

  def self.down
    drop_table :downloads_super_groups
  end
end

