# -*- encoding : utf-8 -*-
class CreateDownloads < ActiveRecord::Migration
  def self.up
    create_table :downloads do |t|
      t.string  :title
      t.string  :language
      
      t.string  :asset_file_name
      t.string  :asset_content_type
      t.integer :asset_file_size
      
      t.timestamps
    end
  end

  def self.down
    drop_table :downloads
  end
end
