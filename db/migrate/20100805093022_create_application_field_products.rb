# -*- encoding : utf-8 -*-
class CreateApplicationFieldProducts < ActiveRecord::Migration
  def self.up
    create_table :application_field_products, :id => false do |t|
      t.integer :application_field_id
      t.integer :product_id
    end
    
    add_index :application_field_products, [:application_field_id, :product_id], :unique => true
  end

  def self.down
    drop_table :application_field_products
  end
end
