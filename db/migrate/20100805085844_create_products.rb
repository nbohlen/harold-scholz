# -*- encoding : utf-8 -*-
class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.integer  :product_group_id
      t.string   :number
      t.integer  :color_name_id
      t.string   :info_de
      t.string   :info_en
      t.string   :shade_1
      t.string   :shade_2
      t.string   :url
      
      t.timestamps
    end
  end

  def self.down
    drop_table :products
  end
end
