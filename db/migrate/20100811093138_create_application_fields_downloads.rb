# -*- encoding : utf-8 -*-
class CreateApplicationFieldsDownloads < ActiveRecord::Migration
  def self.up
    create_table :application_fields_downloads, :id => false do |t|
      t.integer :application_field_id
      t.integer :download_id
    end
    
    add_index :application_fields_downloads, [:application_field_id, :download_id], :unique => true
  end

  def self.down
    drop_table :application_fields_downloads
  end
end
