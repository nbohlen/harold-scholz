# -*- encoding : utf-8 -*-
class CreateApplicationFields < ActiveRecord::Migration
  def self.up
    create_table :application_fields do |t|
      t.integer :parent_id, :null => true, :default => nil 
      t.integer :position
      t.string  :permalink
      
      t.string  :title_de
      t.string  :title_en
      t.text    :text_de
      t.text    :text_en
      t.string  :main_header_de
      t.string  :main_header_en
      t.integer :column_header_1_id, :null => true, :default => nil 
      t.integer :column_header_2_id, :null => true, :default => nil 
      
      t.timestamps
    end
  end

  def self.down
    drop_table :application_fields
  end
end
