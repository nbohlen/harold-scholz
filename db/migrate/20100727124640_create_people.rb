# -*- encoding : utf-8 -*-
class CreatePeople < ActiveRecord::Migration
  def self.up
    create_table :people do |t|
      t.integer  :people_category_id
      t.string   :full_name
      t.string   :designation_de
      t.string   :designation_en
      t.integer  :position
      t.string   :picture_file_name
      t.string   :picture_content_type
      t.integer  :picture_file_size
      t.timestamps
    end
  end

  def self.down
    drop_table :people
  end
end
