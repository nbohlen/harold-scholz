# -*- encoding : utf-8 -*-
class CreateProductGroups < ActiveRecord::Migration
  def self.up
    create_table :product_groups do |t|
      t.string :title_de
      t.string :title_en
      t.string :permalink
      t.text   :text_de
      t.text   :text_en
      
      t.timestamps
    end
  end

  def self.down
    drop_table :product_groups
  end
end
