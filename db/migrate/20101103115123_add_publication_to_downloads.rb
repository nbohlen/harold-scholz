# -*- encoding : utf-8 -*-
class AddPublicationToDownloads < ActiveRecord::Migration
  def self.up
    add_column :downloads, :issue, :string, :nil => true, :default => nil
    add_column :downloads, :publication_date, :date, :nil => true, :default => nil
  end

  def self.down
    remove_column :downloads, :issue
    remove_column :downloads, :publication_date
  end
end
