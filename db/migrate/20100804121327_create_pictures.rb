# -*- encoding : utf-8 -*-
class CreatePictures < ActiveRecord::Migration
  def self.up
    create_table :pictures do |t|
      t.integer :post_id
      t.string  :picture_file_name
      t.string  :picture_content_type
      t.integer :picture_file_size

      t.timestamps
    end
    
    add_index :pictures, :post_id
  end

  def self.down
    drop_table :pictures
  end
end
