# -*- encoding : utf-8 -*-
class CreateJobs < ActiveRecord::Migration
  def self.up
    create_table :jobs do |t|
      t.string :title_de
      t.string :title_en
      t.string :region
      t.text :body_de
      t.text :body_en
      t.boolean :active
      t.string :author
      
      t.timestamps
    end
  end

  def self.down
    drop_table :jobs
  end
end
