# -*- encoding : utf-8 -*-
class AddIdToApplicationFieldProducts < ActiveRecord::Migration
  def self.up
    add_column :application_field_products, :id, :primary_key
  end

  def self.down
    remove_column :application_field_products, :id
  end
end
