# -*- encoding : utf-8 -*-
class CreatePosts < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.string      :author,      :null => true, :default => nil
      t.string      :title_de,    :null => true, :default => nil
      t.string      :title_en,    :null => true, :default => nil
      t.text        :content_de,  :null => true, :default => nil
      t.text        :content_en,  :null => true, :default => nil
      t.date        :date,        :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :posts
  end
end
