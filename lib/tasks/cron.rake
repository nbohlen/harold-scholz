desc "This task is called by the Heroku cron add-on"
task :cron => :environment do
  bundler = HerokuBundler.new(
    ENV['HEROKU_USER'],
    ENV['HEROKU_PASSWORD'],
    ENV['APP_NAME']
  )
  bundler.destroy_all.capture
end