# -*- encoding : utf-8 -*-
class HerokuBundler
  require 'heroku'
  
  def initialize(user, password, app_name)
    @user, @password, @app_name = user, password, app_name
  end
  
  def destroy_all
    client.pgbackups(@app_name).each do |bundle|
      client.pgbackups_destroy(@app_name, pgbackups[:name])
    end
    return self
  end
  
  def capture
    client.pgbackups_capture(@app_name)
    return self
  end
  
  private
  
  def client
    @client ||= Heroku::Client.new(@user, @password)
  end
end
