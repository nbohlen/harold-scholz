# -*- encoding : utf-8 -*-
ActionController::Routing::Routes.draw do |map|
  map.logout '/logout', :controller => 'sessions', :action => 'destroy'
  map.login '/login', :controller => 'sessions', :action => 'new'

  map.resources :users

  map.resource :session

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end
  
  map.resources :people
  map.resources :posts
  map.resources :jobs
  map.resources :downloads
  map.resources :application_fields
  map.resources :super_groups
  
  map.namespace :admin do |admin|
    admin.resources :people, :collection => { :move_higher => :post, :move_lower => :post }
    admin.resources :posts
    admin.resources :jobs
    admin.resources :product_groups
    admin.resources :products
    admin.resources :downloads
    admin.resources :application_fields, :collection => { :move_higher => :post, :move_lower => :post }
    admin.resources :application_field_products
    admin.resources :color_names
    admin.resources :super_groups
  end

  
  map.resource :contact_form
  
  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  map.root :controller => "about", :action => 'index'

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.imprint 'imprint', :controller => "about", :action => "imprint"
  map.contact_us 'contact_us', :controller => "about", :action => "contact"
  map.sustainability 'sustainability', :controller => "about", :action => "sustainability"
  map.company 'company', :controller => 'about', :action => 'company'
  map.history 'history', :controller => 'about', :action => 'history'
  map.sitemap 'sitemap', :controller => 'about', :action => 'sitemap'
  
  
  map.packaging 'packaging', :controller => 'service', :action => 'packaging'
  map.technical_support 'technical_support', :controller => 'service', :action => 'technical_support'
  map.dosing_systems 'dosing_systems', :controller => 'service', :action => 'dosing_systems'
  map.press 'press', :controller => 'service', :action => 'press'
  map.partner 'partner', :controller => 'service', :action => 'partner'
  # map.colour_pigments 'colour_pigments', :controller => 'product_range', :action => "colour_pigments"
  map.carbon_black 'carbon_black', :controller => 'product_range', :action => "carbon_black"
  map.toll_manufacturing 'toll_manufacturing', :controller => 'product_range', :action => "toll_manufacturing"
  map.roofolan 'roofolan', :controller => 'product_range', :action => 'roofolan'
  map.admin '/admin', :controller => "posts", :action => "index", :namespace => "admin"
  
  
  #map.signup  '/signup', :controller => 'users',   :action => 'new'
  map.login  '/login',  :controller => 'session', :action => 'new'
  map.logout '/logout', :controller => 'session', :action => 'destroy'
  
  #connect supergroups
  map.connect '/carbon-black', :controller => 'super_groups', :action => 'show', :id => "carbon-black"
  map.connect '/chromoxid', :controller => 'super_groups', :action => 'show', :id => "chromoxid"
  map.connect '/eisenoxid', :controller => 'super_groups', :action => 'show', :id => "eisenoxid"
  map.connect '/euronox', :controller => 'super_groups', :action => 'show', :id => "euronox"
  map.connect '/euronyl', :controller => 'super_groups', :action => 'show', :id => "euronyl"
  map.connect '/flussigfarben', :controller => 'super_groups', :action => 'show', :id => "flussigfarben"
  map.connect '/fluessigfarben', :controller => 'super_groups', :action => 'show', :id => "flussigfarben"
  map.connect '/slurry', :controller => 'super_groups', :action => 'show', :id => "flussigfarben"
  map.connect '/kasselerbraun', :controller => 'super_groups', :action => 'show', :id => "nussbeize"
  map.connect '/nussbeize', :controller => 'super_groups', :action => 'show', :id => "nussbeize"
  map.connect '/lohnfertigung', :controller => 'super_groups', :action => 'show', :id => "lohnfertigung"
  map.connect '/mischphasenpigmente', :controller => 'super_groups', :action => 'show', :id => "mischphasenpigmente"
  map.connect '/organische-pigmente', :controller => 'super_groups', :action => 'show', :id => "organische-pigmente"
  map.connect '/roofolan', :controller => 'super_groups', :action => 'show', :id => "roofolanr"
  map.connect '/titandioxid', :controller => 'super_groups', :action => 'show', :id => "titandioxid"
  map.connect '/ultramarinblau', :controller => 'super_groups', :action => 'show', :id => "ultramarinblau"
  map.connect '/wismuthvanadat', :controller => 'super_groups', :action => 'show', :id => "wismuthvanadat"
  
  
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
  
  # #catch all routes
  # map.connect '*path', :controller => 'redirect', :action => 'index'
end

#ActionController::Routing::Translator.i18n('de')
ActionController::Routing::Translator.translate_from_file 'config/locales', 'i18n-routes.yml'

