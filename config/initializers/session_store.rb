# -*- encoding : utf-8 -*-
# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_harold-scholz_session',
  :secret      => 'ffca35d40af23a5080115a62c2eb923c7c79e39d176d9d8cee31f20b37645a574cafb54747fb477d83ce1b993a337a640cb1c8b427725c2e70c92a44a1c13a06'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
