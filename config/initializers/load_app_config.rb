# -*- encoding : utf-8 -*-
raw_config = File.read(RAILS_ROOT + "/config/app_config.yml")
APP_CONFIG = YAML.load(raw_config)[RAILS_ENV].symbolize_keys
APP_CONFIG.each do |k, v|
  v.symbolize_keys! if v.respond_to?(:symbolize_keys!)
end
