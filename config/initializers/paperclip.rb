# -*- encoding : utf-8 -*-
# configure Amazon S3 for storage in production
case RAILS_ENV
when 'production'
  Paperclip::Attachment.default_options[:storage] = :s3
  Paperclip::Attachment.default_options[:s3_credentials] = { :access_key_id => ENV['S3_KEY'], :secret_access_key => ENV['S3_SECRET'] }
  Paperclip::Attachment.default_options[:s3_permissions] = 'public-read'
  Paperclip::Attachment.default_options[:bucket] = "production.harold-scholz.de" 
  # Paperclip::Attachment.default_options[:url] = ":s3_path_url"
  Paperclip::Attachment.default_options[:url] = ":s3_alias_url"
  Paperclip::Attachment.default_options[:s3_host_alias] = 'production.harold-scholz.de' # new
  Paperclip::Attachment.default_options[:path] = "/pictures/:id/:basename-:style.:extension"
  Paperclip::Attachment.default_options[:s3_protocol] = "http"
when 'staging'
  Paperclip::Attachment.default_options[:storage] = :s3
  Paperclip::Attachment.default_options[:s3_credentials] = { :access_key_id => ENV['S3_KEY'], :secret_access_key => ENV['S3_SECRET'] }
  Paperclip::Attachment.default_options[:s3_permissions] = 'public-read'
  Paperclip::Attachment.default_options[:bucket] = "staging.harold-scholz.de" 
  Paperclip::Attachment.default_options[:url] = ":s3_path_url"
  Paperclip::Attachment.default_options[:path] = "/pictures/:id/:basename-:style.:extension"
else
  Paperclip::Attachment.default_options[:storage] = :filesystem
  Paperclip::Attachment.default_options[:url]  = "/system/pictures/:id/:basename-:style.:extension" 
  Paperclip::Attachment.default_options[:path] = 'public/system/pictures/:id/:basename-:style.:extension'  
end
