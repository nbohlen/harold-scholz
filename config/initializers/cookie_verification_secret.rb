# -*- encoding : utf-8 -*-
# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.cookie_verifier_secret = 'd169011bce40ca9a02355766e306ff72286dd6be05202c6239a84e29beadcc834b4a3ce2f86985f8a27cc7f2bf0f864a0a3b29292d2d50db91756dd13762d021';
